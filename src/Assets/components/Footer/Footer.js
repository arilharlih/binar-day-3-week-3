import React from "react";
import { Container } from "react-bootstrap";
import "./Footer.css";

export const Footer = () => {
  return (
    <footer>
      <div className="container">
        <div className="section-1">
          <p>CREATED BY ARIL HARLIH</p>
          <p>aurielharlih9213@gmail.com</p>
        </div>
      </div>
    </footer>
  );
};
