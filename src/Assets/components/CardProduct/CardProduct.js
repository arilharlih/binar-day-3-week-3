import React from "react";
import { Card, Button } from "react-bootstrap";
import DummyImg from "../../img/dummy-img.svg";

export const CardProduct = ({ title, desc }) => {
  return (
    <Card style={{ width: "18rem" }}>
      <Card.Img variant="top" src={DummyImg} />
      <Card.Body>
        <Card.Title>{title}</Card.Title>
        <Card.Text>{desc}</Card.Text>
        <Button variant="primary">Go somewhere</Button>
      </Card.Body>
    </Card>
  );
};
