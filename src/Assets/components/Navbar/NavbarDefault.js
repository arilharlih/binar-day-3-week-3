import React from "react";
import { Container, Nav, Navbar } from "react-bootstrap";

export const NavbarDefault = () => {
  return (
    <Navbar bg="dark" variant="dark">
      <Container>
        <Navbar.Brand href="/">Navbar</Navbar.Brand>
        <Nav className="me-auto">
          <Nav.Link href="/">Home</Nav.Link>
          <Nav.Link href="/product">Product</Nav.Link>
          <Nav.Link href="/gallery">Gallery</Nav.Link>
          <Nav.Link href="/contact">Contact Us</Nav.Link>
          <Nav.Link href="/about">About Us</Nav.Link>
          <Nav.Link href="/login">Login</Nav.Link>
          <Nav.Link href="/media">Media</Nav.Link>
          <Nav.Link href="/chart">Chart</Nav.Link>
          <Nav.Link href="/form">Form</Nav.Link>
        </Nav>
      </Container>
    </Navbar>
  );
};
