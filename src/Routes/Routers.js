import React from "react";
import { Route, Routes } from "react-router";
import { AboutUs } from "../Pages/AboutUs/AboutUs";
import { ContactUs } from "../Pages/ContactUs/ContactUs";
import { Gallery } from "../Pages/Gallery/Gallery";
import { Homepage } from "../Pages/Homepage/Homepage";
import { LoginPage } from "../Pages/Login/LoginPage";
import { Product } from "../Pages/Product/Product";
import { Product2 } from "../Pages/Product/Product2";
import { TestPage } from "../Pages/TestPage/TestPage";
import { MediaHandling } from "../Pages/MediaHandling/MediaHandling";
import { ChartPage } from "../Pages/ChartPage/ChartPage";
import { FormPage } from "../Pages/FormPage/FormPage";

export const Routers = () => {
  return (
    <Routes>
      <Route path="/" element={<Homepage />} />
      <Route path="/product/*" element={<Product />} />
      <Route path="/gallery" element={<Gallery />} />
      <Route path="/contact" element={<ContactUs />} />
      <Route path="/about" element={<AboutUs />} />
      <Route path="/login" element={<LoginPage />} />
      <Route path="/media" element={<MediaHandling />} />
      <Route path="/chart" element={<ChartPage />} />
      <Route path="/form" element={<FormPage />} />
      <Route path="test" element={<TestPage />} />
    </Routes>
  );
};
