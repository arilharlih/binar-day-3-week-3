import React from "react";
import ReactPlayer from "react-player";
import ModalImage from "react-modal-image";
import Dropzone from "react-dropzone-uploader";
import { Worker, Viewer } from "@react-pdf-viewer/core";
import "@react-pdf-viewer/core/lib/styles/index.css";
import ProfilePDF from "../../Assets/documents/Profile.pdf";

export const MediaHandling = () => {
  return (
    <div className="d-flex flex-column gap-5 justify-content-center align-items-center min-vh-100">
      {/* <Dropzone
        accept="application/pdf"
        minSizeBytes={1024 * 1024}
        maxSizeBytes={1024 * 1024 * 3}
      /> */}
      <ReactPlayer
        url={"https://www.youtube.com/watch?v=Upqle2SiSxY"}
        playing
        controls
        muted
        onPlay={() => console.log("Video is playing")}
      />
      <ModalImage
        small={
          "https://trakteer.id/storage/images/units/uic-d0MpuiYCVMOR4hyNATEqJ6OdCaERdEH21608475965.png"
        }
        large={"https://i.ytimg.com/vi/5LRAOhPPoAc/maxresdefault.jpg"}
        alt="Chloe Pawapua"
        showRotate
      />
      {/* <Worker workerUrl="https://unpkg.com/pdfjs-dist@2.13.216/build/pdf.worker.min.js">
        <Viewer fileUrl={ProfilePDF} initialPage={1} />
      </Worker> */}
    </div>
  );
};
