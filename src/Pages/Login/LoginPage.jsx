import React from "react";
import GoogleLogin from "react-google-login";
import "./LoginPage.css";

export const LoginPage = () => {
  const handleResponse = (res) => {
    console.log("Response: ", res);
  };

  return (
    <div className="login-page">
      <div className="login-button">
        <GoogleLogin
          clientId={process.env.REACT_APP_GOOGLE_CLIENT_ID}
          buttonText="Login with Google"
          onSuccess={handleResponse}
          onFailure={handleResponse}
          cookiePolicy="single_host_origin"
        />
      </div>
    </div>
  );
};
