import React from "react";
import "chart.js/auto";
import { Bar, Line, Pie } from "react-chartjs-2";
import { faker } from "@faker-js/faker";

export const ChartPage = () => {
  const labels = ["2021", "2022", "2023", "2024", "2025"];
  const datas = {
    labels,
    datasets: [
      {
        label: "Dataset 1",
        data: labels.map(() => faker.datatype.number({ min: 0, max: 1000 })),
        borderColor: "rgb(255, 99, 132)",
        backgroundColor: "rgba(255, 99, 132, 0.5)",
      },
      {
        label: "Dataset 2",
        data: labels.map(() => faker.datatype.number({ min: 0, max: 1000 })),
        borderColor: "rgb(53, 162, 235)",
        backgroundColor: "rgba(53, 162, 235, 0.5)",
      },
    ],
  };

  return (
    <div className="d-flex flex-column gap-5 mt-5 align-items-center min-vh-100">
      <div className="w-50">
        <h4 className="text-center mb-3">Bar</h4>
        <Bar data={datas} />
      </div>
      <div className="w-50">
        <h4 className="text-center mb-3">Line</h4>
        <Line data={datas} />
      </div>
      <div className="w-50">
        <h4 className="text-center mb-3">Pie</h4>
        <Pie data={datas} />
      </div>
    </div>
  );
};
