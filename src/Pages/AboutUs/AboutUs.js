import React from "react";
import { Container } from "react-bootstrap";

export const AboutUs = () => {
  return (
    <div>
      <Container className="d-flex justify-content-center align-items-center min-vh-100">
        <h1>ABOUT</h1>
      </Container>
    </div>
  );
};
