import React, { useEffect, useState } from "react";
import { Card, Alert, Container } from "react-bootstrap";
import DateAdapter from "@mui/lab/AdapterDateFns";
import LocalizationProvider from "@mui/lab/LocalizationProvider";
import { DatePicker, TimePicker } from "@mui/lab";
import { TextField } from "@mui/material";
import { format } from "date-fns";

export const TestPage = () => {
  return (
    <Container className="mt-5">
      <TestDatePicker />
    </Container>
  );
};

const TestDatePicker = () => {
  const [date, setDate] = useState(null);
  const [time, setTime] = useState(null);

  return (
    <div>
      <h2 className="mb-5">Date Picker</h2>
      <LocalizationProvider dateAdapter={DateAdapter}>
        <DatePicker
          label="Basic example"
          value={date}
          onChange={(newValue) => {
            console.log(format(newValue, "dd-MM-yyyy"));
            setDate(newValue);
          }}
          renderInput={(params) => <TextField {...params} />}
        />
      </LocalizationProvider>
      <LocalizationProvider dateAdapter={DateAdapter}>
        <TimePicker
          label="Basic example"
          value={time}
          onChange={(newValue) => {
            console.log(format(newValue, "HH:mm"));
            setTime(newValue);
          }}
          renderInput={(params) => <TextField {...params} />}
        />
      </LocalizationProvider>
    </div>
  );
};

const Todo = () => {
  var axios = require("axios");

  const [data, setData] = useState([]);

  useEffect(() => {
    var config = {
      method: "get",
      url: "https://jsonplaceholder.typicode.com/todos",
      headers: {},
    };

    axios(config)
      .then(function (response) {
        setData(response.data);
      })
      .catch(function (error) {
        console.log(error);
      });
  }, []);

  const createDataElements = (datas) => {
    return datas.map((value) => {
      return (
        <Card style={{ width: "18rem" }} key={value.id}>
          <Card.Body className="d-flex flex-column justify-content-between">
            <Card.Title>{value.title}</Card.Title>
            <Alert
              variant={value.completed ? "success" : "danger"}
              className="m-0"
            >
              <p className="m-0">
                {value.completed ? "Selesai" : "Belum Selesai"}
              </p>
            </Alert>
          </Card.Body>
        </Card>
      );
    });
  };

  return (
    <div className="d-flex flex-column align-items-center mt-5">
      <div className="d-flex gap-5 flex-wrap justify-content-center">
        {createDataElements(data)}
      </div>
    </div>
  );
};
