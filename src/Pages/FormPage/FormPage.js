import React, { useState } from "react";
import { Button, Form } from "react-bootstrap";

export const FormPage = () => {
  // variabel state untuk menyimpan input name
  const [name, setName] = useState("");
  // variabel state untuk menyimpan input category
  const [category, setCategory] = useState("");
  // variabel state untuk menyimpan input harga
  const [price, setPrice] = useState(0);
  // variabel state untuk menyimpan input status
  const [status, setStatus] = useState(false);
  // variabel state untuk menyimpan input image
  const [image, setImage] = useState(null);

  // fungsi yang dijalankan saat menekan tombol submit
  const handleSubmit = () => {
    postDataToApi();
  };

  // fungsi untuk mengirim data ke API
  const postDataToApi = () => {
    console.log("Post data to API...");

    var axios = require("axios");
    var FormData = require("form-data");

    // Form Data yang akan dikirimkan
    var data = new FormData();

    // Menambahkan data name, category, price, status, file image ke Form Data
    data.append("name", name);
    data.append("category", category);
    data.append("price", price);
    data.append("status", status);
    data.append("image", image);

    // Settingan untuk mengirimkan data
    // url: link API
    // data: Form data yang sudah dibuat diatas
    var config = {
      method: "post",
      url: "https://rent-cars-api.herokuapp.com/admin/car",
      data: data,
    };

    // mengirim data ke API menggunakan axios
    axios(config)
      .then(function (response) {
        // menampilkan response API di console jika berhasil
        console.log(JSON.stringify(response.data));
      })
      .catch(function (error) {
        // menampilkan error jika tidak berhasil
        console.log(error);
      });
  };

  return (
    <div className="w-100 mt-5 d-flex justify-content-center">
      <Form className="w-75">
        <Form.Group className="mb-3" controlId="formName">
          <Form.Label>Name</Form.Label>
          {/* Input nama */}
          <Form.Control
            type="text"
            placeholder="Enter car name"
            onChange={(e) => setName(e.target.value)}
          />
        </Form.Group>

        <Form.Group className="mb-3" controlId="formCategory">
          <Form.Label>Category</Form.Label>
          {/* Input category */}
          <Form.Control
            type="text"
            placeholder="Enter Category"
            onChange={(e) => setCategory(e.target.value)}
          />
        </Form.Group>

        <Form.Group className="mb-3" controlId="formPrice">
          <Form.Label>Price</Form.Label>
          {/* Input price */}
          <Form.Control
            type="number"
            placeholder="Enter Price"
            onChange={(e) => setPrice(e.target.value)}
          />
        </Form.Group>

        <Form.Group className="mb-3" controlId="formStatus">
          {/* Input status */}
          <Form.Check
            type="checkbox"
            label="Status"
            onChange={(e) => setStatus(e.target.checked)}
          />
        </Form.Group>

        <Form.Group className="mb-3" controlId="formImage">
          <Form.Label>Image</Form.Label>
          {/* Input image */}
          <Form.Control
            type="file"
            accept="image/png, image/jpeg"
            onChange={(e) => setImage(e.target.files[0])}
          />
        </Form.Group>

        {/* Button Submit */}
        <Button variant="primary" onClick={handleSubmit}>
          Submit
        </Button>
      </Form>
    </div>
  );
};
