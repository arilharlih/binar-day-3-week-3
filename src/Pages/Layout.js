import React from "react";
import { BrowserRouter } from "react-router-dom";
import { Footer } from "../Assets/components/Footer/Footer";
import { NavbarDefault } from "../Assets/components/Navbar/NavbarDefault";
import { Routers } from "../Routes/Routers";

export const Layout = () => {
  return (
    <div>
      <NavbarDefault />
      <BrowserRouter>
        <Routers />
      </BrowserRouter>
      <Footer />
    </div>
  );
};
