import React from "react";
import { Container } from "react-bootstrap";

export const Homepage = () => {
  return (
    <div className="homepage min-vh-100">
      <Container className="d-flex flex-column gap-5 justify-content-center align-items-center min-vh-100">
        <iframe
          width="560"
          height="315"
          src="https://www.youtube.com/embed/0BK43A95qq4"
          title="YouTube video player"
          frameBorder="0"
          allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture"
          allowFullScreen
        ></iframe>
        <h1>SELAMAT DATANG DI HOMEPAGE</h1>
      </Container>
    </div>
  );
};
