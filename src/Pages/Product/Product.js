import React from "react";
import { Container } from "react-bootstrap";
import { BrowserRouter, Routes, Route, useParams } from "react-router-dom";
import { CardProduct } from "../../Assets/components/CardProduct/CardProduct";
import { Product2 } from "./Product2";

export const Product = () => {
  const productData = [
    {
      title: "Product 1",
      desc: "Some quick example text to build on the card title and make up the bulk of the card's content.",
    },
    {
      title: "Product 2",
      desc: "Some quick example text to build on the card title and make up the bulk of the card's content.",
    },
    {
      title: "Product 3",
      desc: "Some quick example text to build on the card title and make up the bulk of the card's content.",
    },
    {
      title: "Product 4",
      desc: "Some quick example text to build on the card title and make up the bulk of the card's content.",
    },
    {
      title: "Product 5",
      desc: "Some quick example text to build on the card title and make up the bulk of the card's content.",
    },
  ];

  return (
    <div className="product-page">
      <Container className="d-flex flex-wrap gap-5 pt-5">
        {productData.map((item) => {
          return (
            <CardProduct key={item.title} title={item.title} desc={item.desc} />
          );
        })}
        <Routes>
          <Route path="/product2" element={<Product2 />} />
        </Routes>
      </Container>
    </div>
  );
};
